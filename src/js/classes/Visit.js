import { request } from '../tools/fetch.js';

export class Visit {
    constructor(url) {
        this.url = url;
    }

    createForm() {
        const formInsert = document.querySelector('.main');
        const formHtml = `
            <div class="modalVisit">
                <div class="modalVisit-content">
                    <div class="modalVisit-header">
                    <h2 class="modalVisit-title">Новий візит</h2>
                    <span class="closeVisit">&times;</span>
                    </div>                
                    <form class="create-visit">
                        <div class="mb-3">
                            <input name='title' class="form-control" type="text" placeholder="Мета візиту" aria-label="default input example" required/>
                        </div>
                        <div class="mb-3">
                            <textarea name='description' class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Короткий опис візиту" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Терміновість</label>
                            <select name='priory' class="form-control" aria-label="Default select example" required>
                                <option value="Звичайна">Звичайна</option>
                                <option value="Пріоритетна">Пріоритетна</option>
                                <option value="Невідкладна">Невідкладна</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <input name='firstname' class="form-control" type="text" placeholder="ПІБ" aria-label="default input example" required/>
                        </div>
                        <div class="mb-3">
                            <select name='doctor' class="form-control select-form" aria-label="Default select example" required>
                            <option value="Виберіть лікаря" selected>Виберіть лікаря</option>
                                <option value="Кардіолог">Кардіолог</option>
                                <option value="Стоматолог">Стоматолог</option>
                                <option value="Терапевт">Терапевт</option>
                            </select>
                        </div>
                        <div class="select-output"></div>
                        <button type="submit" class="btn btn-primary">Створити</button>                        
                    </form>
                </div>
            </div>`;
        formInsert.insertAdjacentHTML('afterbegin', formHtml);

        const testModal = document.querySelector('.button-test');
        const modal = document.querySelector('.modalVisit');

        testModal.addEventListener('click', e => {
            modal.classList.add('active-visit');
        });
    }

    async createVisit(list) {
        const modalForm = document.querySelector('.create-visit');
        const modal = document.querySelector('.modalVisit');

        modalForm.addEventListener('submit', async e => {
            e.preventDefault();

            const { title, description, priory, firstname, doctor, pressure, weight, diseases, age, date } = Object.fromEntries(new FormData(e.target));

            const response = await request({
                url: this.url,
                method: 'POST',
                body: JSON.stringify({
                    firstname: firstname,
                    title: title,
                    description: description,
                    doctor: doctor,
                    pressure: pressure,
                    age: age,
                    weight: weight,
                    priory: priory,
                    diseases: diseases,
                    date: date,
                }),
                headers: { 'Content-Type': 'application/json' },
            });

            list.list.push(response.res);
            const tilesWrap = document.querySelector('.tilesWrap');

            const id = response.res.id;

            let templeteVisit = `<li class="li-item visit-${id}">
            <span class="editVisit editVisit${id}">&#9998;</span>
            <span class="deleteVisit deleteVisit${id}" id="${id}">&times;</span>`;
            templeteVisit += firstname !== '' ? `<h2 class="li-h2 ed-firstname">${firstname}</h2>` : '';
            templeteVisit += doctor !== '' ? `<h3 class="li-h3 ed-doctor">${doctor}</h3>` : '';
            templeteVisit += title !== '' ? `<h3 class="li-text ed-title">Мета: ${title}</h3>` : '';
            templeteVisit += `<div class="li-read-more">`;
            templeteVisit += description !== '' ? `<p class="li-text ed-description">${description}</p>` : '';
            templeteVisit += !!age && age !== '' ? `<p class="li-text ed-age">Вік: ${age}</p>` : '';
            templeteVisit += !!priory && priory !== '' ? `<p class="li-text ed-priory">${priory}</p>` : '';
            templeteVisit += !!date && date !== '' ? `<p class="li-text ed-date">Попереднє відвідування:</br>${date}</p>` : '';
            templeteVisit += !!pressure && pressure !== '' ? `<p class="li-text ed-pressure">Тиск: ${pressure}</p>` : '';
            templeteVisit += !!diseases && diseases !== '' ? `<p class="li-text ed-diseases">Перенесені ЗССС </br>${diseases}</p>` : '';
            templeteVisit += !!weight && weight !== '' ? `<p class="li-text ed-weight">Вага: ${weight}</p>` : '';
            templeteVisit += `</div><button class="li-button">Показати більше</button>
            </li>`;

            tilesWrap.insertAdjacentHTML('afterbegin', templeteVisit);

            const noContent = document.querySelector('.no-content');
            if (tilesWrap.childElementCount > 0) {
                noContent.classList.add('no-content-hidden');
            } else {
                noContent.classList.remove('no-content-hidden');
            }

            modal.classList.remove('active-visit');

            modalForm.reset();

            const editVisit = document.querySelector(`.editVisit${id}`);

            editVisit.addEventListener('click', async e => {
                const editPost = await request({
                    url: this.url + '/' + id,
                    method: 'GET',
                    headers: { 'Content-Type': 'application/json' },
                });

                !!document.querySelector('button.btn') ? document.querySelector('button.btn').remove() : '';
                !!document.querySelector('div.btn') ? '' : modalForm.insertAdjacentHTML('beforeend', '<div class="btn btn-primary">Редагувати</div>');
                const saveBtn = document.querySelector('div.btn-primary');

                if (editPost.status === 200) {
                    modal.classList.add('active-visit');

                    modalForm.querySelector('input[name="firstname"]').value = editPost.res.firstname;
                    modalForm.querySelector('input[name="title"]').value = editPost.res.title;
                    modalForm.querySelector('textarea[name="description"]').value = editPost.res.description;
                    modalForm.querySelector('select[name="priory"]').value = editPost.res.priory;
                    modalForm.querySelector('select[name="doctor"]').value = editPost.res.doctor;
                    try {
                        if (!!editPost.res?.age && editPost.res?.age !== '') {
                            modalForm.querySelector('input[name="age"]').value = editPost.res.age;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.pressure && editPost.res?.pressure !== '') {
                            modalForm.querySelector('input[name="pressure"]').value = editPost.res.pressure;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.weight && editPost.res?.weight !== '') {
                            modalForm.querySelector('input[name="weight"]').value = editPost.res.weight;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.diseases && editPost.res?.diseases !== '') {
                            modalForm.querySelector('textarea[name="diseases"]').value = editPost.res.diseases;
                        } else {
                            ('');
                        }
                    } catch {
                        console.log('err');
                    } finally {
                        if (!!editPost.res?.date && editPost.res?.date !== '') {
                            modalForm.querySelector('input[name="date"]').value = editPost.res.date;
                        } else {
                            ('');
                        }
                    }

                    saveBtn.addEventListener('click', async e => {
                        let edAge;
                        let edPressure;
                        let edWeight;
                        let edDiseases;
                        let edDate;
                        let edFirstname = modalForm.querySelector('input[name="firstname"]').value;
                        let edTitle = modalForm.querySelector('input[name="title"]').value;
                        let edDescription = modalForm.querySelector('textarea[name="description"]').value;
                        let edPriory = modalForm.querySelector('select[name="priory"]').value;
                        let edDoctor = modalForm.querySelector('select[name="doctor"]').value;
                        modalForm.querySelector('input[name="age"]') ? (edAge = modalForm.querySelector('input[name="age"]').value) : '';
                        modalForm.querySelector('input[name="pressure"]') ? (edPressure = modalForm.querySelector('input[name="pressure"]').value) : '';
                        modalForm.querySelector('input[name="weight"]') ? (edWeight = modalForm.querySelector('input[name="weight"]').value) : '';
                        modalForm.querySelector('textarea[name="diseases"]') ? (edDiseases = modalForm.querySelector('textarea[name="diseases"]').value) : '';
                        modalForm.querySelector('input[name="date"]') ? (edDate = modalForm.querySelector('input[name="date"]').value) : '';

                        const edPost = await request({
                            url: this.url + '/' + id,
                            method: 'PUT',
                            body: JSON.stringify({
                                id: id,
                                firstname: edFirstname,
                                title: edTitle,
                                description: edDescription,
                                doctor: edDoctor,
                                pressure: edPressure,
                                age: edAge,
                                weight: edWeight,
                                priory: edPriory,
                                diseases: edDiseases,
                                date: edDate,
                            }),
                            headers: { 'Content-Type': 'application/json' },
                        });
                        if (edPost.status === 200) {
                            !!document.querySelector('div.btn') ? document.querySelector('div.btn').remove() : '';
                            !!document.querySelector('button.btn') ? '' : modalForm.insertAdjacentHTML('beforeend', '<button type="submit" class="btn btn-primary">Створити</button> ');
                            document.querySelector('.ed-firstname').textContent = `${edFirstname}`;
                            document.querySelector('.ed-doctor').textContent = `${edDoctor}`;
                            document.querySelector('.ed-title').textContent = `Мета: ${edTitle}`;
                            document.querySelector('.ed-description').textContent = `${edDescription}`;
                            !!document.querySelector('.ed-age') ? (document.querySelector('.ed-age').textContent = `Вік: ${edAge}`) : '';
                            document.querySelector('.ed-priory').textContent = `${edPriory}`;
                            !!document.querySelector('.ed-date') ? (document.querySelector('.ed-date').textContent = `Попереднє відвідування:</br>${edDate}`) : '';
                            !!document.querySelector('.ed-pressure') ? (document.querySelector('.ed-pressure').textContent = `Тиск: ${edPressure}`) : '';
                            !!document.querySelector('.ed-diseases') ? (document.querySelector('.ed-diseases').textContent = `Перенесені ЗССС </br>${edDiseases}`) : '';
                            !!document.querySelector('.ed-weight') ? (document.querySelector('.ed-weight').textContent = `Вага: ${edWeight}`) : '';

                            modal.classList.remove('active-visit');

                            modalForm.reset();
                        }
                    });
                }
            });

            const deleteVisit = document.querySelector(`.deleteVisit${id}`);

            deleteVisit.addEventListener('click', async e => {
                const deletePost = await request({
                    url: this.url + '/' + id,
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json' },
                });
                if (deletePost.status === 200) {
                    for (let i = 0; i < list.list.length; i++) {
                        if (list.list[i].id == id) {
                            list.list.splice(i, 1);
                        }
                    }

                    const removePost = document.querySelector(`.visit-${id}`);
                    removePost.remove();
                }

                const tilesWrap = document.querySelector('.tilesWrap');
                const noContent = document.querySelector('.no-content');

                if (tilesWrap.childElementCount > 0) {
                    noContent.classList.add('no-content-hidden');
                } else {
                    noContent.classList.remove('no-content-hidden');
                }
            });

            const liButtonReadMore = document.querySelector(`.visit-${id}`).querySelector('.li-button');
            const divReadMore = document.querySelector(`.visit-${id}`).querySelector('.li-read-more');

            liButtonReadMore.addEventListener('click', e => {
                divReadMore.classList.add('li-read-more-on');
                e.target.remove();
            });
        });

        const modalClose = document.querySelector('.closeVisit');
        function loadReset() {
            !!document.querySelector('div.btn') ? document.querySelector('div.btn').remove() : '';
            !!document.querySelector('button.btn') ? '' : modalForm.insertAdjacentHTML('beforeend', '<button type="submit" class="btn btn-primary">Створити</button> ');
            const heart = document.querySelector(".heart")
            const dentic = document.querySelector(".dentic")
            const therapic = document.querySelector(".therapic")
            heart?.remove();
            dentic?.remove();
            therapic?.remove();
        }
        modalClose.addEventListener('click', e => {
            loadReset()
            modalForm.reset();
            modal.classList.remove('active-visit');
        });

        window.addEventListener('click', event => {
            if (event.target == modal) {
                loadReset()
                modalForm.reset()
                modal.classList.remove('active-visit');
            }
        });
    }

    readMore(visits) {
        visits.forEach(item => {
            const id = item.querySelector('.deleteVisit').getAttribute('id');
            const liButtonReadMore = document.querySelector(`.visit-${id}`).querySelector('.li-button');
            const divReadMore = document.querySelector(`.visit-${id}`).querySelector('.li-read-more');
            liButtonReadMore.addEventListener('click', e => {
                divReadMore.classList.add('li-read-more-on');
                e.target.remove();
            });
        });
    }

    editVisit(visits) {
        const modalForm = document.querySelector('.create-visit');
        const modal = document.querySelector('.modalVisit');
        visits.forEach(item => {
            item.querySelector('.editVisit').addEventListener('click', async e => {
                const id = item.querySelector('.editVisit').getAttribute('id');
                const editPost = await request({
                    url: this.url + '/' + id,
                    method: 'GET',
                    headers: { 'Content-Type': 'application/json' },
                });

                !!document.querySelector('button.btn') ? document.querySelector('button.btn').remove() : '';
                !!document.querySelector('div.btn') ? '' : modalForm.insertAdjacentHTML('beforeend', '<div class="btn btn-primary">Редагувати</div>');
                const saveBtn = document.querySelector('div.btn-primary');

                if (editPost.status === 200) {
                    modal.classList.add('active-visit');

                    modalForm.querySelector('input[name="firstname"]').value = editPost.res.firstname;
                    modalForm.querySelector('input[name="title"]').value = editPost.res.title;
                    modalForm.querySelector('textarea[name="description"]').value = editPost.res.description;
                    modalForm.querySelector('select[name="priory"]').value = editPost.res.priory;
                    modalForm.querySelector('select[name="doctor"]').value = editPost.res.doctor;


                    if (modalForm.querySelector('select[name="doctor"]').value === 'Терапевт' && !modalForm.querySelector('input[name="age"]')) {
                        modalForm.querySelector('select[name="doctor"]').insertAdjacentHTML(
                            'afterend',
                            `<div class="therapic">
                            <div class="mb-3">
                        <input class="form-control" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
                    </div>
                    </div>`
                        );
                    }

                    if (modalForm.querySelector('select[name="doctor"]').value === 'Стоматолог' && !modalForm.querySelector('input[name="date"]')) {
                        modalForm.querySelector('select[name="doctor"]').insertAdjacentHTML(
                            'afterend',
                            `<div class="dentic">
                            <div class="mb-3">
                                <label class="form-label">Дата останнього відвідування</label>
                                <input class="form-control" type="date" name="date" aria-label="default input example" required/>
                            </div>
                        </div>`
                        );
                    }

                    if (modalForm.querySelector('select[name="doctor"]').value === 'Кардіолог' && !modalForm.querySelector('input[name="age"]') && !modalForm.querySelector('input[name="pressure"]')) {
                        modalForm.querySelector('select[name="doctor"]').insertAdjacentHTML(
                            'afterend',
                            ` <div class="heart">
                            <div class="mb-3">
                                <input name="pressure" class="form-control" type="text" placeholder="Звичайний тиск" aria-label="default input example" required/>
                            </div>
                            <div class="mb-3">
                                <input class="form-control" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
                            </div>
                            <div class="mb-3">
                                <textarea name="diseases" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
                            </div>
                            <div class="mb-3">
                            <input class="form-control" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
                            </div>
                        </div>`
                        );
                    }

                    try {
                        if (!!editPost.res?.age && editPost.res?.age !== '') {
                            modalForm.querySelector('input[name="age"]').value = editPost.res.age;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.pressure && editPost.res?.pressure !== '') {
                            modalForm.querySelector('input[name="pressure"]').value = editPost.res.pressure;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.weight && editPost.res?.weight !== '') {
                            modalForm.querySelector('input[name="weight"]').value = editPost.res.weight;
                        } else {
                            ('');
                        }
                        if (!!editPost.res?.diseases && editPost.res?.diseases !== '') {
                            modalForm.querySelector('textarea[name="diseases"]').value = editPost.res.diseases;
                        } else {
                            ('');
                        }
                    } catch {
                        console.log('err');
                    } finally {
                        if (!!editPost.res?.date && editPost.res?.date !== '') {
                            modalForm.querySelector('input[name="date"]').value = editPost.res.date;
                        } else {
                            ('');
                        }
                    }
                    const heart = document.querySelector(".heart")
                    const dentic = document.querySelector(".dentic")
                    const therapic = document.querySelector(".therapic")
                    const selected= document.querySelector(".select-form")
                    selected.addEventListener('click', e=>{
                        heart?.remove();
                        dentic?.remove();
                        therapic?.remove();
                    })
                    saveBtn.addEventListener('click', async e => {
                        let edAge;
                        let edPressure;
                        let edWeight;
                        let edDiseases;
                        let edDate;

                        let edFirstname = modalForm.querySelector('input[name="firstname"]').value;
                        let edTitle = modalForm.querySelector('input[name="title"]').value;
                        let edDescription = modalForm.querySelector('textarea[name="description"]').value;
                        let edPriory = modalForm.querySelector('select[name="priory"]').value;
                        let edDoctor = modalForm.querySelector('select[name="doctor"]').value;

                        modalForm.querySelector('input[name="age"]') ? (edAge = modalForm.querySelector('input[name="age"]').value) : '';
                        modalForm.querySelector('input[name="pressure"]') ? (edPressure = modalForm.querySelector('input[name="pressure"]').value) : '';
                        modalForm.querySelector('input[name="weight"]') ? (edWeight = modalForm.querySelector('input[name="weight"]').value) : '';
                        modalForm.querySelector('textarea[name="diseases"]') ? (edDiseases = modalForm.querySelector('textarea[name="diseases"]').value) : '';
                        modalForm.querySelector('input[name="date"]') ? (edDate = modalForm.querySelector('input[name="date"]').value) : '';

                        const edPost = await request({
                            url: this.url + '/' + id,
                            method: 'PUT',
                            body: JSON.stringify({
                                id: id,
                                firstname: edFirstname,
                                title: edTitle,
                                description: edDescription,
                                doctor: edDoctor,
                                pressure: edPressure,
                                age: edAge,
                                weight: edWeight,
                                priory: edPriory,
                                diseases: edDiseases,
                                date: edDate,
                            }),
                            headers: { 'Content-Type': 'application/json' },
                        });
                        if (edPost.status === 200) {
                            !!document.querySelector('div.btn') ? document.querySelector('div.btn').remove() : '';
                            !!document.querySelector('button.btn') ? '' : modalForm.insertAdjacentHTML('beforeend', '<button type="submit" class="btn btn-primary">Створити</button> ');
                            item.querySelector('.ed-firstname').textContent = `${edFirstname}`;
                            item.querySelector('.ed-doctor').textContent = `${edDoctor}`;
                            item.querySelector('.ed-title').textContent = `Мета: ${edTitle}`;
                            item.querySelector('.ed-description').textContent = `${edDescription}`;
                            !!item.querySelector('.ed-age') ? (item.querySelector('.ed-age').textContent = `Вік: ${edAge}`) : '';
                            item.querySelector('.ed-priory').textContent = `${edPriory}`;
                            !!item.querySelector('.ed-date') ? (item.querySelector('.ed-date').textContent = `Попереднє відвідування:</br>${edDate}`) : '';
                            !!item.querySelector('.ed-pressure') ? (item.querySelector('.ed-pressure').textContent = `Тиск: ${edPressure}`) : '';
                            !!item.querySelector('.ed-diseases') ? (item.querySelector('.ed-diseases').textContent = `Перенесені ЗССС </br>${edDiseases}`) : '';
                            !!item.querySelector('.ed-weight') ? (item.querySelector('.ed-weight').textContent = `Вага: ${edWeight}`) : '';


                            modal.classList.remove('active-visit');

                            modalForm.reset();
                        }
                    });
                }
            });
        });
    }

    deleteVisit(visits, list) {
        visits.forEach(item => {
            item.querySelector('.deleteVisit').addEventListener('click', async e => {
                const id = item.querySelector('.deleteVisit').getAttribute('id');
                const deletePost = await request({
                    url: this.url + '/' + id,
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json' },
                });
                if (deletePost.status === 200) {
                    for (let i = 0; i < list.list.length; i++) {
                        if (list.list[i].id == id) {
                            list.list.splice(i, 1);
                        }
                    }

                    const removePost = document.querySelector('.visit-' + id);
                    removePost.remove();
                }

                const tilesWrap = document.querySelector('.tilesWrap');
                const noContent = document.querySelector('.no-content');

                if (tilesWrap.childElementCount > 0) {
                    noContent.classList.add('no-content-hidden');
                } else {
                    noContent.classList.remove('no-content-hidden');
                }
            });
        });
    }
}
