import { Visit } from './Visit.js';

export class VisitCardiologist extends Visit {
    fildsCardiologist = `
            <div class="heart">
                <div class="mb-3">
                    <input name="pressure" class="form-control" type="text" placeholder="Звичайний тиск" aria-label="default input example" required/>
                </div>
                <div class="mb-3">
                    <input class="form-control" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
                </div>
                <div class="mb-3">
                    <textarea name="diseases" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
                </div>
                <div class="mb-3">
                <input class="form-control" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
                </div>
            </div>`;
}
