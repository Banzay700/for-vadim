import { request } from '../tools/fetch.js';

export class List {
    list;
    constructor(url) {
        this.url = url;
    }

    async displayList() {
        const tilesWrap = document.querySelector('.tilesWrap');

        tilesWrap.innerHTML = '';

        const response = await request({
            url: this.url,
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        });

        this.list = response.res;

        if (response.res && response.res.length !== 0) {
            response.res.forEach(card => {
                let templeteVisit = `<li class="li-item visit-${card.id}">
                <span class="editVisit" id="${card.id}">&#9998;</span>
                <span class="deleteVisit" id="${card.id}">&times;</span>`;
                templeteVisit += card.firstname !== '' ? `<h2 class="li-h2 ed-firstname">${card.firstname}</h2>` : '';
                templeteVisit += card.doctor !== '' ? `<h3 class="li-h3 ed-doctor">${card.doctor}</h3>` : '';
                templeteVisit += card.title !== '' ? `<h3 class="li-text ed-title">Мета: ${card.title}</h3>` : '';
                templeteVisit += `<div class="li-read-more">`;
                templeteVisit += card.description !== '' ? `<p class="li-text ed-description">${card.description}</p>` : '';
                templeteVisit += !!card.age && card.age !== '' ? `<p class="li-text ed-age">Вік: ${card.age}</p>` : '';
                templeteVisit += !!card.priory && card.priory !== '' ? `<p class="li-text ed-priory">${card.priory}</p>` : '';
                templeteVisit += !!card.date && card.date !== '' ? `<p class="li-text ed-date">Попереднє відвідування:</br>${card.date}</p>` : '';
                templeteVisit += !!card.bp && card.bp !== '' ? `<p class="li-text ed-pressure">Тиск: ${card.bp}</p>` : '';
                templeteVisit += !!card.diseases && card.diseases !== '' ? `<p class="li-text ed-diseases">Перенесені ЗССС </br>${card.diseases}</p>` : '';
                templeteVisit += !!card.weight && card.weight !== '' ? `<p class="li-text ed-weight">Вага: ${card.weight}</p>` : '';
                templeteVisit += `</div><button class="li-button">Показати більше</button>
                </li>`;

                tilesWrap.insertAdjacentHTML('afterbegin', templeteVisit);
            });
        } else if (response.res?.length === 0) {
            const noContent = document.querySelector('.no-content');
            noContent.classList.remove('no-content-hidden');
        }

        return document.querySelectorAll('.li-item');
    }

    async displayFilteredList(filters) {
        // console.log(this.list)
        const tilesWrap = document.querySelector('.tilesWrap');
        const noContent = document.querySelector('.no-content');
        const noFilterContent = document.querySelector('.no-filter-content');

        noFilterContent.classList.add('no-filter-content-hidden');
        noContent.classList.add('no-content-hidden');
        tilesWrap.innerHTML = '';

        let cardList = [...this.list];

        if (filters.context.length !== 0) {
            cardList = cardList.filter(card => card.title.includes(filters.context) || card.description.includes(filters.context));
        }

        if (filters.priority !== '') {
            cardList = cardList.filter(card => card.priory === filters.priority);
        }

        if (cardList && cardList.length !== 0) {
            cardList.forEach(card => {
                tilesWrap.insertAdjacentHTML('afterbegin', this.templeteVisit(card));
            });
        } else if (cardList.length === 0 && (filters.context.length !== 0 || filters.priority.length !== 0)) {
            console.log('No filter content');
            noFilterContent.classList.remove('no-filter-content-hidden');
        } else {
            noContent.classList.remove('no-content-hidden');
        }

        return document.querySelectorAll('.deleteVisit');
    }

    templeteVisit(card) {
        let templeteVisit = `<li class="li-item visit-${card.id}">
                <span class="editVisit id="${card.id}">&#9998;</span>
                <span class="deleteVisit" id="${card.id}">&times;</span>`;
        templeteVisit += card.firstname !== '' ? `<h2 class="li-h2">${card.firstname}</h2>` : '';
        templeteVisit += card.doctor !== '' ? `<h3 class="li-h3">${card.doctor}</h3>` : '';
        templeteVisit += card.title !== '' ? `<h3 class="li-text">Мета: ${card.title}</h3>` : '';
        templeteVisit += `<div class="li-read">`;
        templeteVisit += card.description !== '' ? `<p class="li-text">${card.description}</p>` : '';
        templeteVisit += !!card.age && card.age !== '' ? `<p class="li-text">Вік: ${card.age}</p>` : '';
        templeteVisit += !!card.priory && card.priory !== '' ? `<p class="li-text">${card.priory}</p>` : '';
        templeteVisit += !!card.date && card.date !== '' ? `<p class="li-text">Попереднє відвідування:</br>${card.date}</p>` : '';
        templeteVisit += !!card.bp && card.bp !== '' ? `<p class="li-text">Тиск: ${card.bp}</p>` : '';
        templeteVisit += !!card.diseases && card.diseases !== '' ? `<p class="li-text">Перенесені ЗССС </br>${card.diseases}</p>` : '';
        templeteVisit += !!card.weight && card.weight !== '' ? `<p class="li-text">Вага: ${card.weight}</p>` : '';
        templeteVisit += `</div><button class="li-button">Показати більше</button>
                </li>`;
        return templeteVisit;
    }
}