import axios from 'axios'

export class User {
    constructor(email, password, name, surname) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

  register() {}

    logout() {
        localStorage.removeItem('token');
    }

    setToken(token) {
        localStorage.setItem('token', token);
        return true;
    }

    getToken() {
        return localStorage.getItem('token');
    }
}